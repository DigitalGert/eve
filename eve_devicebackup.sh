#! /bin/sh
version=0.3

# Todo.
# Optional. an on device restore script?

# Colours!
Reset='\033[0m'
Red='\033[0;91m'         
Green='\033[0;92m'

echo '            ___  __   __   ___  '
echo '           / _ \ \ \ / /  / _ \ '
echo '          |  __/  \ V /  |  __/ '
echo '           \___|   \_/    \___| '
echo '                                '
echo -e "The eve $Green on device $Reset backup.\n"

mainmount="/mnt" # mountpoint for main

savefolder="$mainmount/eve" # Folder where to save the backups
savename="savegames" # name of the backup files

lastbackup="Last backup off device unknown."
if [ -f "$mainmount/eve/eve.conf" ]; then
    source "$mainmount/eve/eve.conf"
    echo " Welcome back, $name"
    if [ ! -z "$lastrun" ] ; then 
     lastbackup="Your last backup off device was on $lastrun"
     fi
fi

count=$( ls $savefolder/*zip 2>/dev/null | wc -l )  # count up to the next zipfile nr
savefile="$savefolder/$savename.$count.zip"

#check if the intended backup file exists and try to recover
if [ -f "$savefile" ] ; then
  mv -f "$savefile" "$savefolder/$savename.$count.old.zip"
  echo "Save $count existed. Moved to $count.old "
fi

echo Backing up $mainmount
echo into $savefile
echo " "

# Exit if savefolder or mainmount don't exist
if [ ! -d $mainmount ]; then
    echo -e "$Red Error. The main mount at $mainmount is not a directory"
    echo -e "Can't backup!"
    echo -e "$Reset "
    echo "You probably need $( mount | grep fat )" 
    echo -e " "
    read -n 1 -p "Press start to close."
    exit
    fi
if [ ! -d $savefolder ]; then
    echo -e "$Red Error. The backup directory $savefolder is not a directory"
    echo -e "Can't backup!"
    echo -e "$Reset "
    echo "To back up next to the script, use $(pwd)." 
    echo -e " "
    read -n 1 -p "Press start to close."
    exit
    fi
    

echo -e "\n\n"
echo "Warning. This makes it easier to copy"
echo "your savegames to your PC, but still lives"
echo "on your SD card. If it crashes, "
echo 'these files also dissapear!!'
echo -e "\n"
echo -e "$Red ! Please copy the backups to safer storage ! $Reset"
echo -e "\n\n"

echo Collecting your saves might take a moment
echo  before the zipping starts

cd $mainmount

find . -name '*.sav*' \
  -o \
  -name '*.gqs*' \
  -o \
  -name '*.svs*' \
  -o \
  -name '*.cfg*' \
  -o \
  -path './.bittboy-msx/save/*' \
  -o \
  -path './.bittboy-msx/save/*' \
  -o \
  -path './.fceux/cfg/*' \
  -o \
  -path './.fceux/fcs/*' \
  -o \
  -path './.gambatte/saves/*' \
  -o \
  -path './.ngpcemu/sstates/*' \
  -o \
  -path './.ohboy/saves/*' \
  -o \
  -path './.oswan/*' \
  -o \
  -path './.picodrive/mds/*' \
  -o \
  -path './.pocketsnes/*' \
  -o \
  -path './.smsplus/state/*' \
  -o \
  -path './.snes96_snapshots/*' \
  -o \
  -path './.snes9*/*' \
  -o \
  -path './.temper/save_states/*' \
  -o \
  -path './emus/pcsx_rearmed/.pcsx/sstates/*' \
  | zip $savefile -@
backupresult=$?

echo -e "\n\n"

if [ $backupresult -eq 0 ] ;
then 
  echo -e "$Green Savegames saved into $savefile"
  echo -e " ! Please copy the backups to safer storage ! $Reset"
  echo $lastbackup
else
  echo -e  "$Red The backup produced an error $backupresult."
  echo -e " Your savegames were likely not properly saved. $Reset"
fi

echo -e "$Reset "
read -n 1 -p "Press start to close."

