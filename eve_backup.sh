#! /bin/bash

# Plans
# Todo. Double check usability and naming for device source and destination backup dir.
# Todo. Headless mode; no questions asked.
# Option. Blunt backup EVERYTHING   aka rsync -rb --progress --size-only .. ..
# Option. Check for the script version on source and propose to run that if newer

# Get some info
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# check and create the config
source $SCRIPTPATH/config.sh $1 

# script version!
version=0.5

# Run the backups

echo -e "\n 📂 Run global filetypes backups"

rsync -rm --info=name \
  --include='*/'  \
  --include='*.sav*' \
  --include='*.gqs*' \
  --include='*.svs*' \
  --include='*.cfg*' \
  --exclude="*" \
  "$source/" "$destination/$name"


echo -e "\n 📂 Run the exception directories backups"

rsync -rm --info=name \
  --include='*/'  \
  --include='.bittboy-msx/save/*' \
  --include='eve/*' \
  --include='.fceux/cfg/*' \
  --include='.fceux/fcs/*' \
  --include='.gambatte/saves/*' \
  --include='.ngpcemu/sstates/*' \
  --include='.ohboy/saves/*' \
  --include='.oswan/*' \
  --include='.picodrive/mds/*' \
  --include='.pocketsnes/*' \
  --include='.smsplus/state/*' \
  --include='.snes96_snapshots/*' \
  --include='.snes9*/*' \
  --include='.temper/save_states/*' \
  --include='emus/pcsx_rearmed/.pcsx/sstates/*' \
  --exclude="*" \
  "$source/" "$destination/$name"



# Write the run dates to the config file
/bin/cat <<EOM >>"$source/eve/eve.conf"

# EVE backup
lastversion="$version"
lastrun="$(date)"
EOM

# If not run from $source; install or update the eve on device.
echo -e "\n"
bash $SCRIPTPATH/install.sh $source

echo -e "\n 🟢 $0 completed succesfully. "

