#! /bin/sh
version=0.1

# Find what games have savegames
# create items in favorites to start each game that has savegames

# Todo. Add more emulators
# Todo. Customise colours per emu to match the original image
# Todo. Clean up the old favorites
# Todo. Ask the user to confirm to generate these. Requires very fluid input tho.
# Todo. Make the generated image PRETTY. Pango is limited tho :/
# Todo. Make a menu image for the generator
# Bug? Barely reproducable. Some games crash when loading the save state from the other menu item and vice versa. Tho most are ok.

# Colours!
Reset='\033[0m'
Red='\033[0;91m'         
Green='\033[0;92m'

# The main mount point.
mainmount="/mnt"

# Exit if mainmount doesn't exist
if [ ! -d $mainmount ]; then
    echo -e "$Red Error. The main mount at $mainmount is not a directory"
    echo -e "$Reset "
    echo "You probably need $( mount | grep fat )" 
    echo -e " "
    read -n 1 -p "Press start to close."
    exit
    fi

if [ -f "$mainmount/eve/eve.conf" ]; then
    source "$mainmount/eve/eve.conf"
fi

# Menu folder
sections="gmenu2x/sections"
favorites="$sections/favorites"


# Create a favorite menu file if the parameter is passed
if [ "$1" = "createFavorite" ]; then
 savefile="$2"
 gamefile="${savefile%.*}.gba"
 
 if [ ! -f "$gamefile" ] ; then
  echo "Skipping. Can't confirm $gamefile exists."
  exit
  fi
 
 directory=${savefile%/*}
 savefilename=$( basename "$savefile" ) 
 game=${savefilename%.*}

 echo "Generating $game"

 pango-view -t "
                                     <span foreground='black' weight='bold'>eve Favorites

                                       $name</span>




<span foreground='black'>$directory</span> 

<span foreground='#fffefe' weight='bold' size='xx-large'>$game</span>" --markup --background=#9b00c8 --height=240 --width=320 --pixels -q -o "$mainmount/$favorites/$game.png"

 cat > "$mainmount/$favorites/$game"  <<EOF
title=$game
description=$game on gba
exec=/mnt/emus/gpsp_rumble/gpsp_rumble.elf
clock=702
selectorbrowser=false
params="$gamefile"

backdrop=$mainmount/$favorites/$game.png
EOF

 exit
fi



# If there is no parameter, run the start.
echo '            ___  __   __   ___  '
echo '           / _ \ \ \ / /  / _ \ '
echo '          |  __/  \ V /  |  __/ '
echo '           \___|   \_/    \___| '
echo '                                '
echo -e "The eve $Green on device $Reset favorites.\n"
echo -e "You play it, we'll put it your favorites section! \n"


mkdir -p $mainmount/$favorites

 # add $mainmount/ on device
find $mainmount/roms/GBA -name '*sav' -exec $0 createFavorite "{}" \;

echo -e "\n\n"
echo "Your games have been generated into the Favorites section!"
echo "You now have $(( $( ls $mainmount/$favorites | wc -l ) / 2)) favorites!"



echo -e "$Reset "
read -n 1 -p "Press start to close."

