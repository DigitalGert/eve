# Bash scripts easier backups of emulation save games.
*aka The eve backup bash thingy*

For backing up only savegame files from MiyooCFW and similar systems on SD cards. 

*Why tho?* An origin story. I got myself and the family some of those awesome little devices that run MiyooCFW on SD cards. Which is tremendous to play all those games that are locked away on dusty cartridges in the attic. And with some modern day convenience like save states to boot. Sadly tho, these SD cards are fragile things and I would't want anyone to lose hours of play time over an SD crash. So we needed backups! But I also don't want to back up everything each time, the shorter and to the point it is, the more likely to use. And preferably in an automated raspberry pi thingy where all can just plug in for a second and walk away.
Time for a quick bit of bash scripting ^^ 


Do let me know any bugs or features you might think of in the git issues section.


# On Device.
This script is made to run on your MiyooCFW device. It wil back up all your savegames into a fresh .zip file on your SD. Instead of chasing down all backup files, you now have one handy file to copy onto your computer for savegames backup.

! These backup zip files still live on your SD. !
It will be lost when the card crashes. Please backup these onto different storage frequently.

This bash script runs in MiyooCFW st. Please make sure it's available at the standard /mnt/apps/st/st location for the script to work. The settings option, depending on your skin, will look like the st terminal app.

## Installing
There are 3 options to install the on device part.
* Run the off device backup. The script will backup and then install to your sd.
* Manually run the installer. 
> ./install.sh /mount/sd/folder
* Manually copy the files. (as described below)

To manually install..
* copy this ./eve directory to main/eve  
* copy the ./eve/evebackup menu file to main/gmenu2x/sections/settings/evebackup 


# Off Device
These scripts run on your computer with bash (linux native / Windows Subsystem for Linux) and copy the save games and eve backup data from your device to a folder on your computer.

Made to run frequently and read previous settings

## usage
Just insert the SD card,  run ./eve_backup.sh and answer the questions

On a second run, consider pointing to the SD card mount directory for less questions
> bash ./eve_backup.sh /media/gert/main

To restore, run eve_restore.sh and answer the questions.
Or point to the backup location
> bash ./eve_restore.sh /home/gert/backup


# Files
The included files and what they do are detailed in the manifesto file.

# Future
Notes on the next steps are on top of the scripts.


## Philosophy
Some thoughts about some decisions..

* It's annoying to run the backup manually on device.

Ultimately automagical is always better. But this is a game device first and foremost. It could run automatically in the background, at boot or at shutdown. But neither of those work for me. At boot I want it fast so I can get my game time in. At shutdown I want it to safely shut down asap so I can save battery and I don't want to risk it dying mid backup. While playing, I don't want my device to spend cycles on anything but the game I'm playing.
So ultimately, the only viable option is manually.


* Why bash, windows doesn't do that out of the box?

Because I'm on Linux and I like bash ^^ Windows users can run the off device scripts in the Windows Subsystem for Linux or install bash for windows. Or only use the on device part :)
Also, Feel free to create an update for better cross os fun.

* Why all the gum complexity?

I wanted prettier scripts and I wanted to play with gum at the next opportunity. And I like it, it's pretty. I agree tho it's an annoying dependency and that's why it's been moved to an optional part of the config script.

That said. Your life could be prettier! Get gum! :D https://github.com/charmbracelet/gum
And figlet while you're at it. For sweet sweet ascii art!


# References
* https://github.com/TriForceX/MiyooCFW 


# Attribution
Menu image icons 
* Backup icons created by Freepik - https://www.flaticon.com/free-icons/backup
* Paper icons created by torskaya - https://www.flaticon.com/free-icons/paper
* Heart icons created by Freepik - https://www.flaticon.com/free-icons/heart
* Refresh icons created by ghufronagustian - https://www.flaticon.com/free-icons/refresh


# Contact
If need be, i'm at http://about.gertschepens.be ;)
