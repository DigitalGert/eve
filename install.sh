#! /bin/bash
version=0.1

# Get some info

# get sdmount from parameter
sdmount=$1 

# ToDo. if no sdmount $1 parameter, run config.sh

# check sdmount
if [ ! -d "$sdmount" ]; then
  echo Error. ⭕️ The path doesn\'t appear to be your sd mount directory: $sdmount
  exit 1 
fi
echo Installing eve into $sdmount


# Make sure we're in the script directory
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH

# Don't install onto yourself when run from sdmount
if [ $SCRIPTPATH == "$sdmount/eve" ]; then
  echo "Running eve from device sd. Skipping the installation."
else
  if [ ! -f manifesto ]; then
     echo Error. ⭕️ Manifesto file not found.
     echo Please make sure all eve files are in this folder.
  fi

  if [ ! -d "$sdmount/eve" ]; then
    mkdir "$sdmount/eve"
  fi

  # Copy the files to the SD
  while read -r file separator comment; do
     echo "Copying $file. ($comment)";
     cp -f "$file" "$sdmount/eve/"
  done < manifesto
fi

#Todo. Make installing the menu items configurable.

# install the menu item
if [ -d "$sdmount/gmenu2x/sections/settings/" ]; then
   echo Installing the gmenu2x menu item.
   cp -f "evebackup" "$sdmount/gmenu2x/sections/settings/"
else
   echo " "
   echo Error. ⭕️ "$sdmount/gmenu2x/sections/settings/" not found.
   echo The on device script will not be available in the menu.
   echo You can run it manually at ./eve/eve_devicebackup.sh
   echo " "
fi


# install the menu item
if [ -d "$sdmount/gmenu2x/sections/settings/" ]; then
   echo Installing the gmenu2x favorites.
   mkdir -p "$sdmount/gmenu2x/sections/favorites"
   cp -f "evefavorites" "$sdmount/gmenu2x/sections/favorites/"
else
   echo " "
   echo Error. ⭕️ "$sdmount/gmenu2x/sections/" not found.
   echo The on device script will not be available in the menu.
   echo You can run it manually at ./eve/eve_favoritescategory.sh
   echo " "
fi




# It's done :)
echo Eve installer to $sdmount completed.

