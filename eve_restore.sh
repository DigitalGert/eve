#! /bin/bash

# Plans
# Todo. Double check usability and naming for device source and destination backup dir.
# Todo. Headless mode; no questions asked.


# Get some info
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# check and create the config
source $SCRIPTPATH/config.sh $1 

# script version!
version=0.2

if [ ! -d "$source" ]; then
    format "* Error. ⭕️ Can't find the device folder." "Please check the data source path."
    exit
fi

if [ ! -d "$destination/$name" ]; then
    format "* Error. ⭕️ Can't find the $name backup folder." \
    "Please double check the path in which your backups are stored."
    exit
fi

echo Restoring backup from $lastrun
# Todo. Ask for confirmation?


# Run the restore
echo -e "\n 📂 Restoring your backup! From  $destination/$name/ to  $source"
rsync -rm --info=name \
  $destination/$name/  $source



# Write the settings to the config file
/bin/cat <<EOM >>$source/eve/eve.conf

# EVE Restore
lastrestoreversion="$version"
lastrestore="$(date)"
EOM

echo -e "\n Restore completed."

