#! /bin/bash
version=0.1

# Plans
# Optional. Fix source question code duplication
# Optional. Offer config values as defaults when editing

gum=true
if ! command -v gum &> /dev/null ;
then
  gum=false
fi

figlet=true
if ! command -v figlet &> /dev/null ;
then
  figlet=false
fi

# $1 placeholder
# $2 prompt
question () {
  q_answer=""
  if $gum ;
  then 
    q_answer=$(gum input --placeholder="$1" --prompt="$2")
  else
    read -p "$2 [$1]  " q_answer
  fi
}

# $1 Question
# $2 Affirmative
# $3 Negative
confirm () {
  c_answer=""
  if $gum ;
  then 
    gum confirm "$1" --affirmative="$2" --negative="$3"  && c_answer=true
  else
    echo $1
    echo "Press Y to '$2', or N to '$3'"
    read -p "$2? [N]  " c_read
    if [[ "$c_read" =~ [Yy] ]]; then c_answer=true; fi
  fi
}

# $1 $2 .. $n  line
format () {
  if $gum ;
  then 
     gum format "$@"
  else
    echo " "
    for a in "$@";
      do 
      echo "  $a"
    done 
  fi
}

# $1 Big
showbig () {
  if $figlet ;
  then
    figlet $1
    echo ""
  else
    echo ! !  $1 " " $1 " " $1 " " $1 " " $1  ! !
  fi
}

# get source from parameter
source=$1 

# get source
if [ ! -d "$source" ]; then
  default="/media/$(whoami)/main"
  question "$default" "Source: "
  source="$q_answer" 
  source=${source:-$default}
fi
format "Source: $source"

if [ ! -d "$source" ]; then
    format "* Error. ⭕️ Can't find the source folder." "Please check the data source path." "The source needs to be present to save the config to."
    exit
fi

if [ -f "$source/eve/eve.conf" ]; then
    source "$source/eve/eve.conf"
    showbig "$name"
    format "Source: $source" "Destination: $destination" "Name: $name" "Last backup on $lastrun"
    confirm "We found these on your device." "Continue" "Edit"
    config=$c_answer 
fi


# get source
if [ ! "$config" ]; then
  default="/media/$(whoami)/main"
  question "$default" "Source: "
  source="$q_answer" 
  source=${source:-$default}
fi
format "Source: $source"

if [ ! -d "$source" ]; then
    format "* Error. ⭕️ Can't find the source folder." "Please check the data source path." "The source needs to be present to save the config to."
    exit
fi

# get destination
if [ ! "$config" ]; then
  default="/home/$(whoami)/PowKiddy/backup"
  question "$default" "Destination: "
  destination=$q_answer
  destination=${destination:-$default}
  format "Destination: $destination"
fi

if [ ! -d "$destination" ]; then
    format "* Error. ⭕️ Can't find the destination folder." \
    "To fix, consider executing:" "mkdir -p $destination "
    exit
fi

if [ ! $config ]; then
 # get player name. Because who ISN't running multiple players on these ^^
 default=$(whoami)
 question "Player name here [$default]" "Name: "
 name=$q_answer
 name=${name:-$default}
 format "Name: $name"
fi



# Write the settings to the config file
if [ ! -d "$source/eve" ]; then  mkdir "$source/eve"; fi

/bin/cat <<EOM >"$source/eve/eve.conf"
# EVE backup config file. https://gitlab.com/DigitalGert/eve
name="$name"

source="$source"
destination="$destination"

configversion="$version"
lastconfig="$(date)"

EOM


if [ -f "$source/eve/eve.conf" ]; then
  format " 🟢 config file written to $source/eve/eve.conf"
else
  format "❕ failed to write config file to $source/eve/eve.conf"
fi

